require './range_list'

class RangeListTest

  def test_all
    test_normal
    test_exception
  end

  def test_normal
    rl = RangeList.new
    rl.add([1, 5])
    assert_equal(rl.print, '[1, 5)')
    rl.add([10, 20])
    assert_equal(rl.print, '[1, 5) [10, 20)')
    rl.add([10, 20])
    assert_equal(rl.print, '[1, 5) [10, 20)')
    rl.add([20, 20])
    assert_equal(rl.print, '[1, 5) [10, 20)')
    rl.add([20, 21])
    assert_equal(rl.print, '[1, 5) [10, 21)')
    rl.add([2, 4])
    assert_equal(rl.print, '[1, 5) [10, 21)')
    rl.add([3, 8])
    assert_equal(rl.print, '[1, 8) [10, 21)')
    rl.remove([10, 10])
    assert_equal(rl.print, '[1, 8) [10, 21)')
    rl.remove([10, 11])
    assert_equal(rl.print, '[1, 8) [11, 21)')
    rl.remove([15, 17])
    assert_equal(rl.print, '[1, 8) [11, 15) [17, 21)')
    rl.remove([3, 19])
    assert_equal(rl.print, '[1, 3) [19, 21)')
  end

  def test_exception
    rl_add = RangeList.new
    begin
      rl_add.add(nil)
      puts "error"
    rescue => e
      assert_equal(e.class, RangeList::ParamsError)
      assert_equal(e.message, 'Parameter length must be 2')
    end

    begin
      rl_add.add([5])
      puts "error"
    rescue => e
      assert_equal(e.class, RangeList::ParamsError)
      assert_equal(e.message, 'Parameter length must be 2')
    end

    begin
      rl_add.add([5, '2'])
      puts "error"
    rescue => e
      assert_equal(e.class, RangeList::ParamsError)
      assert_equal(e.message, 'Parameter must be integer array')
    end

    begin
      rl_add.add([2, 4, 8])
      puts "error"
    rescue => e
      assert_equal(e.class, RangeList::ParamsError)
      assert_equal(e.message, 'Parameter length must be 2')
    end

    rl_remove = RangeList.new
    begin
      rl_remove.remove(nil)
      puts "error"
    rescue => e
      assert_equal(e.class, RangeList::ParamsError)
      assert_equal(e.message, 'Parameter length must be 2')
    end

    begin
      rl_remove.remove([5, '2'])
      puts "error"
    rescue => e
      assert_equal(e.class, RangeList::ParamsError)
      assert_equal(e.message, 'Parameter must be integer array')
    end

    begin
      rl_remove.remove([5])
      puts "error"
    rescue => e
      assert_equal(e.class, RangeList::ParamsError)
      assert_equal(e.message, 'Parameter length must be 2')
    end

    begin
      rl_remove.remove([3, 6, 8])
      puts "error"
    rescue => e
      assert_equal(e.class, RangeList::ParamsError)
      assert_equal(e.message, 'Parameter length must be 2')
    end
  end

  private
  
  def assert_equal(args1, args2)
    if args1 == args2
      puts "."
    else
      puts "error"
    end
  end
end

RangeListTest.new.test_all